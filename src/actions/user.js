import * as types from '../constants/ActionTypes'


export const setUserData = (data) =>{
    const { token, user } = data;

    localStorage.setItem(types.AUTH_TOKEN,token);
    localStorage.setItem(types.USER_EMAIL,user.email);
    localStorage.setItem(types.USER_ID,user.id);

    return {
        type: types.SET_USER_DATA,
        data
    }
}

export const setUserDetails = (data) =>{
    const { me } = data;
    return {
        type: types.SET_USER_DETAILS,
        me
    }
}

export const updateTicketAmount = (data) =>{

    return {
        type: types.UPDATE_TICKET_AMOUNT,
        data
    }
}

export const removeUserData = () =>{

    localStorage.removeItem(types.AUTH_TOKEN);
    localStorage.removeItem(types.USER_EMAIL);
    localStorage.removeItem(types.USER_ID);

    return {
        type: types.RESET_USER_DATA
    }
}
