/**
 * Mocking client-server processing
 */
import _products from './data.json'
import {useQuery} from "@apollo/react-hooks";
import {Query} from "@apollo/react-components";
import {GET_PRODUCT_ALL} from "../constants/queries";


const TIMEOUT = 100

export default {
    getProducts: (cb, timeout) => {
        // console.log(data);
        // const { loading, error, data } = Query(GET_PRODUCT_ALL, {});
        return setTimeout(() => cb(_products), timeout || TIMEOUT)
    },
    buyProducts: (payload, cb, timeout) => setTimeout(() => cb(), timeout || TIMEOUT)
}
