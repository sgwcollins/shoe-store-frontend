
import gql from "graphql-tag";

export let API_HOST = '';

switch(window.location.hostname){
    case 'localhost':
        API_HOST =  'http://127.0.0.1:8000/graphql/'

}

export const GET_STRIPE_SESSION_ID = gql`
    mutation createPaymentSession($environment: String!, $input: PaymentSessionInput!) {
        createPaymentSession(
            environment:$environment,
            input:$input
        ){
            payment{
                token
            }
            errors{
                field
                message
            }
            
        }
    }`;

export const SESSION_CHECK = gql`
    query paymentCheckSession($sessionId: String!,$checkoutId:String!) {
        paymentCheckSession(
            sessionId:$sessionId,
            checkoutId:$checkoutId,
        ){
            sessionIsCompleted
        }
    }`;

export const COMPLETE_CHECKOUT = gql`
    mutation checkoutComplete($checkout: ID!, ) {
        checkoutComplete(
            checkoutId:$checkout,
        ){
            order {
                id
                status
            }
            errors{
                field
                message
            }

        }
    }`;



export const SHIPPING_UPDATE = gql`
    mutation checkoutShippingMethodUpdate(
        $checkoutId: ID!,
        $shippingMethodId: ID!
    ) {
        checkoutShippingMethodUpdate(
            checkoutId: $checkoutId
            shippingMethodId: $shippingMethodId
        ) {
            checkout {
                id
                totalPrice {
                    gross {
                        amount
                        currency
                    }
                }
            }
            
            errors {
                field
                message
            }
        }
    }
`
export const CHECKOUT_CREATE = gql`
    mutation checkoutCreate(
        $input: CheckoutCreateInput!,
    ) {
        checkoutCreate(
            input:$input
        ){
            checkout {
                id
                token
                totalPrice {
                    gross {
                        amount
                        currency
                    }
                }
                isShippingRequired
                availableShippingMethods {
                    id
                    name
                    price {
                        amount
                    }
                }
                availablePaymentGateways {
                    name
                    config {
                        field
                        value
                    }
                }
            }
            errors{
                field
                message
            }

        }
    }
`;

export const CREATE_RAFFLE_TRANSCATION = gql`
    mutation raffleTransaction(
        $productId: ID!, 
        $ticketAmount: Int!,
        $userId: ID!
    ) {
        raffleTransaction(
            productId:$productId,
            ticketAmount:$ticketAmount,
            userId:$userId
        ){
            product{
                id
                name
                styleId
                isRaffleMode
                ticketAmount
                ticketsLeft
                raffleEndDate
                variants{
                    name
                    images{
                        url
                    }
                }
                isAvailable
                description
                pricing {
                    onSale
                    priceRange {
                        start {
                            gross {
                                amount
                                currency
                            }
                            net{
                                amount
                                currency
                            }
                        }
                    }
                    discount {
                        gross {
                            amount
                            currency
                        }
                    }
                    priceRangeUndiscounted {
                        start {
                            gross {
                                amount
                                currency
                            }
                        }
                    }
                }
                images{
                    url
                }
                thumbnail {
                    url
                }
            }
            user{
                email
                id
                ticketAmount
            }
            errors{
                field
                message
            }

        }
    }
`;


export const GET_PRODUCT_PRICER = gql`
    mutation productPricer($styleId: String!) {
        productPricer(
            styleId:$styleId,
        ){
        response
        errors{
            field
            message
        }
            
        }
    }
`;

export const UPDATE_TICKETS = gql`
    mutation updateTicket(
        $ticketAmount:Int!, $type: String!) {
        updateTicket(
            ticketAmount:$ticketAmount,
            type:$type,
            ) {
            errors {
                field
                message
            }
            user {
                ticketAmount
            }
        }
    }
`


export const GET_CUSTOMER_DETAILS = gql`
   query Me {
       shop{
           countries{
               country
               code
           }
       }
       me{
           firstName
           lastName
           email
           ticketAmount
           usersRaffles{
               id
               name
               id_Count
               raffleEndDate
               ticketsLeft
               ticketAmount
               thumbnail{
                   url
               }
           }
           orders(first:100){
               edges{
                   node{
                       id
                       number
                       paymentStatus
                       created
                       total{
                           net{
                               amount
                           }
                       }
                   }
               }
           }
           defaultBillingAddress{
               streetAddress1
               postalCode
               city
               countryArea
               country{
                   code
                   country
               }
           }
           defaultShippingAddress{
               streetAddress1
               postalCode
               city
               countryArea
               country{
                   code
                   country
               }
           }
       }
   }
`;

export const GET_COUNTRIES = gql`
    query Me {
        shop{
            countries{
                country
                code
            }
        }
    }
`;

export const GET_PRODUCT = gql`
    query getProduct($id: ID!) {
        product(id:$id){
            id
            name
            styleId
            isRaffleMode
            ticketAmount
            ticketsLeft
            raffleEndDate
            variants{
                id
                name
                stockQuantity
                images{
                    url
                }
                
            }
            isAvailable
            description
            pricing {
                onSale
                priceRange {
                    start {
                        gross {
                            amount
                            currency
                        }
                        net{
                            amount
                            currency
                        }
                    }
                }
                discount {
                    gross {
                        amount
                        currency
                    }
                }
                priceRangeUndiscounted {
                    start {
                        gross {
                            amount
                            currency
                        }
                    }
                }
            }
            images{
                url
            }
            thumbnail {
                url
            }
        }
    }
`

export const GET_ALL_COLLECTIONS = gql`    
    query {
        collections(first: 100) {
            edges {
                node 
                    name
                    id
                
            }
        }
    }
`;

export const GET_ALL_PRODUCTS = gql`    
    query {
        products(first: 100, filter:{isPublished: true}) {
            edges {
                node {
                    id
                    name
                    description
                    isAvailable
                    styleId
                    isRaffleMode
                    ticketAmount
                    raffleEndDate
                    ticketsLeft
                    pricing {
                        onSale
                        priceRange {
                            start {
                                gross {
                                    amount
                                    currency
                                }
                                net{
                                    amount
                                    currency
                                }
                            }
                        }
                        discount {
                            gross {
                                amount
                                currency
                            }
                        }
                        priceRangeUndiscounted {
                            start {
                                gross {
                                    amount
                                    currency
                                }
                            }
                        }
                    }
                    thumbnail {
                        url
                    }
                }
            }
        }
    }
`;

export const GET_TOP_PRODUCTS = gql`
    query {
        products(first: 20,filter: { collections: "Top Collection" }) {
            edges {
                node {
                    id
                    name
                    description
                    isAvailable
                    isRaffleMode
                    ticketsLeft
                    ticketAmount
                    raffleEndDate
                    pricing {
                        onSale
                        priceRange {
                            start {
                                gross {
                                    amount
                                    currency
                                }
                                net{
                                    amount
                                    currency
                                }
                            }
                        }
                        discount {
                            gross {
                                amount
                                currency
                            }
                        }
                        priceRangeUndiscounted {
                            start {
                                gross {
                                    amount
                                    currency
                                }
                            }
                        }
                    }
                    thumbnail {
                        url
                    }
                }
            }
        }
    }
`;

export const SIGNUP_MUTATION = gql`    
  mutation accountRegisterMutation($email: String!, $password: String!) {
       accountRegister(input:{email:$email,password:$password}){
                user {
                  email
                  firstName
                  lastName
                  ticketAmount
                  defaultBillingAddress{
                      streetAddress1
                      postalCode
                      city
                      country{
                        code
                        country
                      }
                  }
                  defaultShippingAddress{
                      streetAddress1
                      postalCode
                      city
                      country{
                        code
                        country
                      }
                  }
                }
                errors {
                  field
                  message
                }
              }
          }
`

export const LOGIN_MUTATION = gql`
        mutation TokenCreateMutation($email: String!, $password: String!) {
            tokenCreate(email: $email,password:$password){
              token
                user {
                  email
                  id
                  ticketAmount
           
                }
                errors {
                  field
                  message
                }
              }
          }
  `


export const ACCOUNT_MUTATION = gql`    
    mutation AccountUpdateMutation(
        $firstName: String,$lastName: String, $billing:AddressInput) {
        accountUpdate(
            input: {
                firstName: $firstName,
                lastName: $lastName,
                defaultShippingAddress: $billing
                defaultBillingAddress: $billing
            }) {
            errors {
                field
                message
            }
            user {
                firstName
                lastName
                email
                defaultBillingAddress {
                    id
                }
                defaultShippingAddress {
                    id
                }
            }
        }
    }
`
