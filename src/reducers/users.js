import * as types from '../constants/ActionTypes'


const initialState = {
    authenticated: localStorage.getItem(types.USER_ID) ? true : false,
    userData: {
        email:localStorage.getItem(types.USER_EMAIL) ? localStorage.getItem(types.USER_EMAIL)  : '',
        id:localStorage.getItem(types.USER_ID) ? localStorage.getItem(types.USER_ID)  : '',
        firstName:'',
        defaultBillingAddress:{},
        defaultShippingAddress:{},
        lastName:'',
        ticketAmount:0
    }
};


const userReducer = (state = initialState, action) => {
    switch (action.type) {

        case types.SET_USER_DATA:
            return{
                ...state,
                authenticated: true,
                userData:action.data.user || action.me
            }

        case types.RESET_USER_DATA:
            return{
                ...state,
                authenticated: false,
                userData:{
                    email:'',
                    id:''
                }
            }
        case types.SET_USER_DETAILS:

            return{

                ...state,
                userData:{
                    ...state.userData,
                    firstName:action.me.firstName,
                    lastName:action.me.lastName,
                    ticketAmount:action.me.ticketAmount,
                    defaultBillingAddress:action.me.defaultBillingAddress,
                    defaultShippingAddress:action.me.defaultShippingAddress,

                }
            }

        case types.UPDATE_TICKET_AMOUNT:
            console.log(action.data);
            return{
                ...state,
                userData:{
                    ...state.userData,
                    ticketAmount:action.data.user.ticketAmount,

                }
            }



        default:
            return state;
    }
};
export default userReducer;
