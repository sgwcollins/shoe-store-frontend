import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from "react-redux";
import {removeUserData} from '../../../../actions/user'
import store from '../../../../store/index'
import { withTranslate } from 'react-redux-multilingual'

class TopBar extends Component {

    render() {


        const logout = () =>{
            store.dispatch(removeUserData());
            this.props.history.push(`/`)
        }

        const {translate,email} = this.props;
        return (
            <div className="top-header">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6">
                        </div>
                        <div className="col-lg-6 text-right">
                            <ul className="header-dropdown">
                                <li className="mobile-wishlist compare-mobile"><Link to={`${process.env.PUBLIC_URL}/compare`}><i className="fa fa-random" aria-hidden="true"></i>{translate('compare')}</Link></li>
                                <li className="mobile-wishlist"><Link to={`${process.env.PUBLIC_URL}/wishlist`}><i className="fa fa-heart" aria-hidden="true"></i>{translate('wishlist')}</Link></li>
                                <li className="onhover-dropdown mobile-account">
                                    {email ? (
                                        <React.Fragment>
                                            <i className="fa fa-user" aria-hidden="true"></i> {email}

                                                <ul className="onhover-show-div">
                                                    <li>
                                                        <Link to={`${process.env.PUBLIC_URL}/pages/dashboard`} data-lng="en">Settings</Link>
                                                    </li>
                                                    <li>
                                                        <a onClick={logout} data-lng="en">Logout</a>
                                                    </li>
                                                </ul>
                                        </React.Fragment>
                                    ) : (
                                        <React.Fragment>
                                        <i className="fa fa-user" aria-hidden="true"></i> {translate('my_account')}
                                            <ul className="onhover-show-div">
                                                <li>
                                                    <Link to={`${process.env.PUBLIC_URL}/pages/login`} data-lng="en">Login</Link>
                                                </li>
                                                <li>
                                                    <Link to={`${process.env.PUBLIC_URL}/pages/register`} data-lng="en">Register</Link>
                                                </li>
                                            </ul>
                                        </React.Fragment>
                                    )}

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        email: state.user.userData.email,
    }
}


export default connect(mapStateToProps)(withTranslate(TopBar));
