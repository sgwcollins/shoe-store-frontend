import React, {Component} from 'react';
import {Helmet} from 'react-helmet'
import Slider from 'react-slick';
import '../common/index.scss';
import {connect} from "react-redux";


import { Query } from 'react-apollo';
import {GET_PRODUCT} from  '../../constants/queries'


// import custom Components
import Service from "./common/service";
import BrandBlock from "./common/brand-block";
import NewProduct from "../common/new-product";
import Breadcrumb from "../common/breadcrumb";
import DetailsWithPrice from "./common/product/details-price";
import DetailsWithRaffle from "./common/product/detail-price-raffle";
import DetailsTopTabs from "./common/details-top-tabs";
import {addToCart, addToCartUnsafe, addToWishlist, receiveProduct} from '../../actions'
import ImageZoom from './common/product/image-zoom'
import SmallImages from './common/product/small-image'
import store from "../../store";



class LeftSideBar extends Component {

    constructor() {
        super();
        this.state = {
            open:false,
            nav1: null,
            nav2: null
        };
    }

    // document.getElementById('idOfElement').classList.add('newClassName');


    componentDidMount() {
        this.setState({
            nav1: this.slider1,
            nav2: this.slider2
        });
    }

    filterClick() {
        document.getElementById("filter").style.left = "-15px";
    }
    backClick() {
        document.getElementById("filter").style.left = "-36a5px";
    }

    render(){
        const {symbol, item, addToCart, addToCartUnsafe, addToWishlist,product} = this.props;
        const id = this.props.match.params.id;
        var products = {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            fade: true
        };
        var productsnav = {
            slidesToShow: 3,
            swipeToSlide:true,
            arrows: false,
            dots: false,
            focusOnSelect: true
        };

        return (
            <div>
                {/*SEO Support*/}

                <Query
                    query={GET_PRODUCT}
                    variables={{ id }}
                >
                {({ loading, error, data }) => {

                    if (loading) {
                        document.getElementById("color").setAttribute("href", `#` )
                        return null
                    }
                    if (error) return `Error! ${error}`;

                    store.dispatch(receiveProduct(data.product));


                    return (
                        <>
                        <Helmet>
                            <title>{product.name}</title>
                        </Helmet>

                        <Breadcrumb  parent={'Product'} title={product.name} />

                        {product ?
                            <>
                            <section className="section-b-space">
                                <div className="collection-wrapper">
                                    <div className="container">
                                        <div className="row">
                                            <div className="col-lg-12 col-sm-12 col-xs-12">
                                                <div className="">
                                                    <div className="row">
                                                        <div className="col-xl-12">
                                                            <div className="filter-main-btn mb-2">
                                                        <span onClick={this.filterClick}  className="filter-btn" >
                                                            <i className="fa fa-filter" aria-hidden="true"></i> filter</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-lg-6 product-thumbnail">
                                                            <Slider {...products} asNavFor={this.state.nav2} ref={slider => (this.slider1 = slider)} className="product-slick">
                                                                {product.images.map((vari, index) =>
                                                                        <div key={index}>
                                                                            <ImageZoom image={vari.url} />
                                                                        </div>
                                                                    )}
                                                            </Slider>
                                                            <SmallImages item={product} settings={productsnav} navOne={this.state.nav1} />
                                                        </div>
                                                        {product.isRaffleMode && (
                                                            <DetailsWithRaffle {...this.props} symbol={symbol} item={product} navOne={this.state.nav1} addToCartClicked={addToCart} BuynowClicked={addToCartUnsafe} addToWishlistClicked={addToWishlist} />
                                                        )}
                                                        {!product.isRaffleMode && (
                                                            <DetailsWithPrice {...this.props} symbol={symbol} item={product} navOne={this.state.nav1} addToCartClicked={addToCart} BuynowClicked={addToCartUnsafe} addToWishlistClicked={addToWishlist} />
                                                        )}
                                                   </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            </> : ''}

                    </>);
                }}
                </Query>
            </div>

        )
    }
}

const mapStateToProps = (state, ownProps) => {
    let productId = ownProps.match.params.id;
    return {
        item: state.data.products.find(el => el.id == productId),
        symbol: state.data.symbol,
        product: state.data.product
    }
}

export default connect(mapStateToProps, {addToCart, addToCartUnsafe, addToWishlist}) (LeftSideBar);
