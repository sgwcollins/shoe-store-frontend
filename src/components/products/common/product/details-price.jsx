import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import Slider from 'react-slick';
import Modal from 'react-responsive-modal';
import {RemoteTable} from './table';
import {Countdown} from './countdown';
import moment from 'moment';
import {connect} from "react-redux";
import {CREATE_RAFFLE_TRANSCATION} from "../../../../constants/queries";
import { Mutation } from '@apollo/react-components';
import store from "../../../../store";
import {receiveProduct} from "../../../../actions";
import {updateTicketAmount} from "../../../../actions/user";


class DetailsWithPrice extends Component {

    constructor (props) {
        super (props);
        this.state = {
            open:false,
            quantity:0,
            stock: 'InStock',
            nav3: null,
            activeSize:null
        }
    }

    componentDidMount() {
        this.setState({
            nav3: this.slider3
        });
    }

    minusQty = () => {
        if(this.state.quantity > 1) {
            this.setState({stock: 'InStock'})
            this.setState({quantity: this.state.quantity - 1})
        }
    }


    plusQty = () => {
        const {item} = this.props;
        const {activeSize} = this.state;

        if(item.variants && activeSize !== null){
            if(item.variants[activeSize].stockQuantity >= this.state.quantity){
                this.setState({quantity: this.state.quantity+1})
            }
        }
    }
    changeQty = (e) => {
        this.setState({ quantity: parseInt(e.target.value) })
    }
    changeSize = (i) => {
        this.setState({ activeSize: parseInt(i),quantity:0 })
    }

    render (){
        const {symbol, item, addToCartClicked, BuynowClicked} = this.props;
        const {activeSize} = this.state;

        return (
                    <div className="col-lg-6 rtl-text">
                        <div className="product-right">
                            <h2> {item.name} </h2>

                            <h3>{symbol}{item.pricing.priceRange.start.net.amount} </h3>



                            {item.description && (
                                <>
                                    <div className="border-product">
                                    <p>{item.description}</p>
                                    </div>
                                </>
                            )}

                                <div className="product-description border-product">

                                  {(item.isAvailable &&
                                    <>
                                        {(item.variants) &&
                                            <div>
                                                <h6 className="product-title size-text">select size </h6>
                                                <div className="size-box">
                                                    <ul>
                                                        {item.variants.map((size, i) => {
                                                            return <li
                                                                key={i}
                                                                onClick={()=>this.changeSize(i)}
                                                            >
                                                                <a className={activeSize ===  i ? 'active' : '' }>{size.name}</a>
                                                            </li>
                                                        })}
                                                    </ul>
                                                </div>
                                            </div>
                                        }

                                        <h6 className="product-title">quantity</h6>
                                        <div className="qty-box">
                                            <div className="input-group">
                                                  <span className="input-group-prepend">
                                                    <button type="button" className="btn quantity-left-minus" onClick={this.minusQty} data-type="minus" data-field="">
                                                     <i className="fa fa-angle-left"></i>
                                                    </button>
                                                  </span>
                                                <input type="text" name="quantity" value={this.state.quantity} onChange={this.changeQty} className="form-control input-number" />
                                                <span className="input-group-prepend">
                                                <button type="button" className="btn quantity-right-plus" onClick={this.plusQty} data-type="plus" data-field="">
                                                <i className="fa fa-angle-right"></i>
                                                </button>
                                               </span>
                                            </div>
                                        </div>
                                        <br/>
                                        <div className="product-buttons" >
                                            <a className="btn btn-solid" onClick={() => addToCartClicked(item, this.state.quantity,activeSize)}>add to cart</a>
                                            <Link to={`${process.env.PUBLIC_URL}/checkout`} className="btn btn-solid" onClick={() => BuynowClicked(item, this.state.quantity,activeSize)} >buy now</Link>
                                        </div>
                                    </>
                              )}
                              {(!item.isAvailable &&
                               <div>
                                   OUT OF STOCK!
                               </div>
                              )}


                            <RemoteTable item={item} />
                        </div>
                    </div>
                    </div>
                )}
}

const mapStateToProps = (state) => {
    return{
        user: state.user,
    }
}

export default connect(
    mapStateToProps, {}
)(DetailsWithPrice)


// export default DetailsWithPrice;
