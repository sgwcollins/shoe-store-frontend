import React, {Component} from 'react';
import Breadcrumb from "../common/breadcrumb";
import { useQuery } from '@apollo/react-hooks';
import {GET_ORDERS} from '../../constants/queries';
import {USER_ID} from "../../constants/ActionTypes";

import {isEmpty} from 'lodash'
import {Link} from "react-router-dom";
import moment from "moment";

function Tickets(
    {
        ticketAmount,
        tickets,
        reloadTickets,
    }) {


    return (
        <React.Fragment>
            <div className="row">
                <div className="col-lg-12 mb-4">
                    <h3>Your Ticket Balance: {ticketAmount}</h3>
                    <button className={'btn btn-solid'} onClick={reloadTickets}>Reload Ticket</button>
                </div>

                <div className="col-lg-12">
                    <table className="table cart-table table-responsive-xs">
                        <thead>
                        <tr className="table-head">
                            <th scope="col"></th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Amount of Tickets</th>
                            <th scope="col">Raffle End Date</th>
                        </tr>
                        </thead>
                        <tbody>

                        {tickets.map((data) => {
                            return (
                                <tr>
                                    <td>
                                        <img src={data.thumbnail.url} />

                                    </td>
                                    <td>
                                        {data.name}

                                    </td>
                                    <td>

                                        {data.id_Count}
                                    </td>
                                    <td>
                                        {moment(data.raffleEndDate).fromNow()}
                                    </td>

                                </tr>
                            )
                        })}
                        </tbody>

                    </table>


                </div>

            </div>

        </React.Fragment>
    )

}

export default Tickets
