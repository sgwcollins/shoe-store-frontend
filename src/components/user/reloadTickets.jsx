import React, { useState,useEffect } from 'react';
import { useMutation } from '@apollo/react-hooks';
import {GET_STRIPE_SESSION_ID,UPDATE_TICKETS} from  '../../constants/queries'
import {ticketValue} from  '../../constants/util'
import {loadStripe} from '@stripe/stripe-js';
import { useForm } from 'react-hook-form'
import queryString from 'query-string'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import store from "../../store";
import {updateTicketAmount} from "../../actions/user";



const ReloadTickets = (props) =>  {

    console.log(props);
    const ticketAmount  = useSelector(state => state.user.userData.ticketAmount);
    const {history, location } = props;

    const [tickets, setTickets] =  useState(0);
    const { register, handleSubmit,errors } = useForm();

    const [getSession] = useMutation(GET_STRIPE_SESSION_ID);
    const [updateTickets] = useMutation(UPDATE_TICKETS);


    console.log('test');
    let values = queryString.parse(window.location.search)

    console.log(values);
    if('payment' in values){
        console.log(' -- payment -- ');
        history.push({
            pathname:location.pathname,
            search:''
        })

        if(values['payment'] === '1'){

            updateTickets({ variables: {
                    type: 'add',
                    ticketAmount: sessionStorage.getItem('quantity')
            }}).then((data)=>{
                store.dispatch(updateTicketAmount(data.data.updateTicket));
            })


        }

        sessionStorage.removeItem('quantity')


    }

    const  onSubmit = async formData => {
        const stripe = await loadStripe(process.env.REACT_APP_STRIPE_API_KEY);
        sessionStorage.setItem('quantity',formData['ticketAmount']);
        let sessionData = await getSession({ variables: {
            environment: process.env.REACT_APP_ENV,
            input: {
                amount:ticketValue,
                quantity:formData['ticketAmount'],
                name:"Raffle Tickets",
                description:"Raffle Tickets",
                currency:"USD",
                successUrl:window.location.protocol+'//'+window.location.hostname+''+location.pathname+'?payment=1',
                cancelUrl:window.location.protocol+'//'+window.location.hostname+''+location.pathname
            }

        }})

        stripe.redirectToCheckout({
            sessionId: sessionData.data.createPaymentSession.payment.token
        }).then(function (result) {
            // If `redirectToCheckout` fails due to a browser or network
            // error, display the localized error message to your customer
            // using `result.error.message`.
        });

    }



    const onError = (err) => {
        console.log("Error!", err);
    }


    return (
            <React.Fragment>
                <div className="row">
                    <div className="col-lg-12">
                        <h2>Reload Your Ticket</h2>
                        <h3>Current Balance: <span style={{color:'green'}}>{ticketAmount}</span></h3>
                    </div>
                    <form onSubmit={handleSubmit(onSubmit)}>
                     <div className="col-lg-12">
                        <div className="form-group">
                            <strong>Choose amount</strong>
                            <button
                                type="button"
                                className="btn tck-btn"
                                onClick={()=>{
                                    setTickets(5)
                                }}>
                                5
                            </button>
                            <button
                                type="button"
                                className="btn tck-btn"
                                onClick={()=>{
                                    setTickets(10)
                                }}>
                                10
                            </button>
                            <button
                                type="button"
                                className="btn tck-btn"
                                onClick={()=>{
                                    setTickets(15)
                                }}>
                                15
                            </button>

                            <button
                                type="button"
                                className="btn tck-btn"
                                onClick={()=>{
                                    setTickets(20)
                                }}>
                                20
                            </button>
                        </div>

                        <div className="form-group">
                            <input
                                type="number"
                                ref={register({ min: 1, max: 300 })}
                                className="form-control"
                                id="ticket"
                                value={tickets}
                                name="ticketAmount"
                                placeholder="Ticket Amount"
                                onChange={(event)=>{
                                    setTickets(event.target.value);
                                }} />

                            {errors.ticketAmount && 'Ticket amount must be between 0 - 300'}
                        </div>

                        <div className="form-group">
                                <button type="submit" className="btn-solid btn" >Place Order</button>
                        </div>


                    </div>
                    </form>

                </div>

            </React.Fragment>
    )

};

export default ReloadTickets
