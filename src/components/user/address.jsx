import React, {Component} from 'react';
import Breadcrumb from "../common/breadcrumb";
import { useQuery } from '@apollo/react-hooks';
import {GET_CUSTOMER_DETAILS} from '../../constants/queries';
import {USER_ID} from "../../constants/ActionTypes";

import {isEmpty} from 'lodash'

function Address({address,openModal}) {

    let rtnAddress = Object.assign({}, address);
    let streetAddress1 = '';
    let postalCode = '';
    let city = '';
    let country = '';
    let countryCode = '';

    if(!isEmpty(rtnAddress)) {
        streetAddress1 = rtnAddress.streetAddress1.toUpperCase();
        postalCode = rtnAddress.postalCode.toUpperCase();
        city = rtnAddress.city.toUpperCase();
        country = rtnAddress.country.country.toUpperCase();
        countryCode = rtnAddress.country.code;
    }



    return (
        <React.Fragment>

        {!isEmpty(rtnAddress) &&
            <div>
                <address>
                    {streetAddress1} <br/>
                    {postalCode}  {city} <br/>
                    {countryCode},  {country} <br/>
                </address>
            </div>
        }
        {isEmpty(rtnAddress) &&
            <address>
                You have not set a default shipping address.<br/>
            </address>
        }

        <a onClick={()=>{openModal('address')}}>Edit Address</a>
        </React.Fragment>
    )

}

export default Address
