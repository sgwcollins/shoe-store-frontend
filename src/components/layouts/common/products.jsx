import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import {connect} from 'react-redux'

import {getBestSeller, getMensWear, getWomensWear} from '../../../services/index'
import {addToCart, addToWishlist, addToCompare} from "../../../actions/index";
import ProductItem from './product-item';

class SpecialProducts extends Component {
    render (){

        const {bestSeller,mensWear,womensWear, symbol, addToCart, addToWishlist, addToCompare,items:{products:{edges}} } = this.props

        console.log(edges);
        return (
            <div>
                <div className="title1 section-t-space">
                    <h4>exclusive products</h4>
                    <h2 className="title-inner1">special products</h2>
                </div>
                <section className="section-b-space p-t-0">
                    <div className="container">

                                <div className="no-slider row">
                                    { edges.map((product, index ) =>
                                        <ProductItem product={product.node} symbol={symbol}
                                                     onAddToCompareClicked={() => addToCompare(product.node)}
                                                     onAddToWishlistClicked={() => addToWishlist(product.node)}
                                                     onAddToCartClicked={() => addToCart(product.node, 1)} key={index} /> )
                                    }
                                </div>

                    </div>
                </section>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    bestSeller: getBestSeller(state.data.products),
    mensWear: getMensWear(state.data.products),
    womensWear: getWomensWear(state.data.products),
    symbol: state.data.symbol
})

export default connect(mapStateToProps, {addToCart, addToWishlist, addToCompare}) (SpecialProducts);
