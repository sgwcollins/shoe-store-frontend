import React, { Component } from 'react';
import {Helmet} from 'react-helmet'
import '../../common/index.scss';
import Slider from 'react-slick';
import {Link} from 'react-router-dom';

// Import custom components
import TopCollection from './top-collection';
import SpecialProducts from "../common/products";
import BlogSection from "../common/blogsection";
import Instagram from "../common/instagram";
import LogoBlock from "../common/logo-block";
import {
    svgFreeShipping,
    svgservice,
    svgoffer
} from "../../../services/script"

import { Query } from 'react-apollo';
import {GET_ALL_PRODUCTS, GET_TOP_PRODUCTS} from  '../../../constants/queries'
import store from "../../../store";
import {storeAllProducts} from "../../../actions";



class Fashion extends Component {

    componentDidMount() {

        setTimeout(function () {
            document.querySelector(".loader-wrapper").style = "display: none";
        }, 2000);

        document.getElementById("color").setAttribute("href", `#` );
    }

	render() {

        return (
			<div>
                <Helmet>
                    <title>Revendeur</title>
                    <meta name="description" content="E-Commerce Sneaker Store" />
                </Helmet>

                {/*Home Slider*/}
                <section className="p-0">
                    <div  className="slide-1 home-slider">
                        <div>
                            <div className="home home1 text-center">
                                <div className="home-overlay"></div>
                                    <div className="container">
                                    <div className="row">
                                        <div className="col">
                                            <div className="slider-contain">
                                                <div>
                                                    <h4>welcome to Revendeur</h4>
                                                    <h1>men fashion</h1>
                                                    <Link to={`${process.env.PUBLIC_URL}/left-sidebar/collection`} className="btn btn-solid">shop now</Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                {/*Home Section End*/}

                <Query query={GET_TOP_PRODUCTS}>
                    {({ loading, error, data }) => {

                        if (loading) {
                            document.getElementById("color").setAttribute("href", `#` )
                            return null
                        }
                        if (error) return `Error! ${error}`;

                        document.getElementById("color").setAttribute("href", `#` );
                        return (
                            <>
                                <TopCollection type={'Top Collection'} items={data} />

                            </>

                        );
                    }}
                </Query>



                    {/*Parallax banner*/}
                    <section className="p-0">
                        <div className="full-banner parallax-banner1 parallax text-center p-left">
                            <div className="container">
                                <div className="row">
                                    <div className="col">
                                        <div className="banner-contain">
                                            <h2>2018</h2>
                                            <h3>fashion trends</h3>
                                            <h4>special offer</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    {/*Parallax banner End*/}
                <Query query={GET_ALL_PRODUCTS}>
                    {({ loading, error, data }) => {
                        if (loading) {
                            document.getElementById("color").setAttribute("href", `#` )
                            return null
                        }
                        if (error) return `Error! ${error}`;
                        document.getElementById("color").setAttribute("href", `#` );

                        console.log(data);
                        return (
                            <>
                                <SpecialProducts items={data} />
                            </>

                        );
                    }}
                </Query>


                <Instagram />


			</div>
			)


	}
}

export default Fashion;
