import React, {Component} from 'react';
import { withTranslate } from 'react-redux-multilingual'
import { withRouter} from 'react-router-dom';

// Custom Components
import HeaderOne from './common/headers/header-one';
import FooterOne from "./common/footers/footer-one";


class App extends Component {


    render() {
        return (
            <div>
                <HeaderOne {...this.props} logoName={'logo.jpg'}/>
                {this.props.children}
                <FooterOne logoName={'logo.jpg'}/>


            </div>
        );
    }
}

export default withTranslate(App);
