import React, {Component} from 'react';
import {Helmet} from 'react-helmet'
import { connect } from 'react-redux'
import PaypalExpressBtn from 'react-paypal-express-checkout';
import SimpleReactValidator from 'simple-react-validator';
import { Query } from 'react-apollo';
import {GET_CUSTOMER_DETAILS,CHECKOUT_CREATE,GET_COUNTRIES,SHIPPING_UPDATE,GET_STRIPE_SESSION_ID} from '../../constants/queries';
import Breadcrumb from "../common/breadcrumb";
import {removeFromWishlist} from '../../actions'
import {getCartTotal} from "../../services";
import {AddressCard} from "./addressCard";
import {ShippingOptions} from "./ShippingOptions";
import { Mutation } from '@apollo/react-components';
import {ticketValue} from "../../constants/util";
import {loadStripe} from "@stripe/stripe-js";

class checkOut extends Component {

    constructor (props) {
        super (props);

        this.state = {
            firstName:'',
            lastName:'',
            email:'',
            countryCode:'',
            streetAddress1:'',
            city:'',
            state:'',
            showCheckout:false,
            showContact:false,
            showCheckoutButton:true,
            shippingOptionId:'',
            checkoutId:'',
            checkoutToken:'',
            shippingOptions:[],
            errors:''
        }
        this.validator = new SimpleReactValidator();
    }

    setStateFromInput = (event) => {
        var obj = {};
        obj[event.target.name] = event.target.value;
        this.setState(obj);

      }

    checkhandle(value) {
        this.setState({
            payment: value
        })
    }

    StripeClick = () => {

        if (this.validator.allValid()) {
            alert('You submitted the form and stuff!');

            var handler = (window).StripeCheckout.configure({
                key: 'pk_test_glxk17KhP7poKIawsaSgKtsL',
                locale: 'auto',
                token: (token: any) => {
                    console.log(token)
                      this.props.history.push({
                          pathname: '/order-success',
                              state: { payment: token, items: this.props.cartItems, orderTotal: this.props.total, symbol: this.props.symbol }
                      })
                }
              });
              handler.open({
                name: 'Multikart',
                description: 'Online Fashion Store',
                amount: this.amount * 100
              })
        } else {
          this.validator.showMessages();
          // rerender to show messages for the first time
          this.forceUpdate();
        }
    }

    render (){
        const {cartItems, symbol, total, user} = this.props;

        const {showContact,shippingOptions,showCheckoutButton,checkoutId,shippingOptionId,checkoutToken} = this.state;

        // Paypal Integration
        const onSuccess = (payment) => {
            console.log("The payment was succeeded!", payment);
            this.props.history.push({
                pathname: '/order-success',
                    state: { payment: payment, items: cartItems, orderTotal: total, symbol: symbol }
            })

        }

        const onCancel = (data) => {
            console.log('The payment was cancelled!', data);
        }


        const  createMarkup = (string) =>{
            return {
                __html: string   };
        };

        const onError = (err) => {
            console.log("Error!", err);
        }

        const client = {
            sandbox:    'AZ4S98zFa01vym7NVeo_qthZyOnBhtNvQDsjhaZSMH-2_Y9IAJFbSD3HPueErYqN8Sa8WYRbjP7wWtd_',
            production: 'AZ4S98zFa01vym7NVeo_qthZyOnBhtNvQDsjhaZSMH-2_Y9IAJFbSD3HPueErYqN8Sa8WYRbjP7wWtd_',
        }

        return (
            <div>

                {/*SEO Support*/}
                <Helmet>
                    <title>Rvndr | Sneaker Store</title>
                    <meta name="description" content="Rvndr | Sneaker Store" />
                </Helmet>
                {/*SEO Support End */}

                <Breadcrumb  title={'Checkout'}/>
                <Query query={(user.authenticated) ? GET_CUSTOMER_DETAILS : GET_COUNTRIES} >
                    {({ loading, error, data }) => {
                        if (loading) return 'Loading...';
                        if (error) {
                            console.log('No user data found')
                        }

                        const {shop:{countries}, me}= data;




                        return (
                            <section className="section-b-space">
                                <div className="container padding-cls">
                                    <div className="checkout-page">
                                        <div className="checkout-form">
                                            <form>
                                                <div className="checkout row">
                                                    <div className="col-lg-6 col-sm-12 col-xs-12">
                                                        <div className="checkout-title">
                                                            <h3>
                                                                Shipping Details
                                                            </h3>
                                                        </div>

                                                        {this.state.errors &&(
                                                            <div
                                                                className="errors"
                                                                dangerouslySetInnerHTML={createMarkup(this.state.errors)} />
                                                        )}

                                                        {user.authenticated && (
                                                            <div>
                                                                <AddressCard data={data} />
                                                            </div>
                                                        )}
                                                        {!user.authenticated  && (
                                                            <>
                                                            <div className="row check-out">
                                                            <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                            <div className="field-label">First Name</div>
                                                            <input type="text" name="firstName"
                                                            value={this.state.firstName}
                                                            onChange={this.setStateFromInput}/>
                                                            {this.validator.message('firstName', this.state.firstName, 'required|alpha')}
                                                            </div>
                                                            <div className="form-group col-md-6 col-sm-6 col-xs-12">
                                                            <div className="field-label">Last Name</div>
                                                            <input type="text" name="lastName"
                                                            value={this.state.lastName}
                                                            onChange={this.setStateFromInput}/>
                                                            {this.validator.message('lastName', this.state.lastName, 'required|alpha')}
                                                            </div>
                                                            <div className="form-group col-md-12 col-sm-12 col-xs-12">
                                                            <div className="field-label">Email Address</div>
                                                            <input type="text" name="email" value={this.state.email}
                                                            onChange={this.setStateFromInput}/>
                                                            {this.validator.message('email', this.state.email, 'required|email')}
                                                            </div>
                                                            <div className="form-group col-md-12 col-sm-12 col-xs-12">
                                                            <div className="field-label">Country</div>
                                                            <select
                                                                name="countryCode"
                                                                value={this.state.countryCode}
                                                                onChange={this.setStateFromInput}>
                                                                <option key=''></option>
                                                                {countries.map((data) => (
                                                                    <option key={data['code']} value={data['code']}>{data['country']}</option>
                                                                ))}
                                                            </select>
                                                            {this.validator.message('countryCode', this.state.countryCode, 'required')}
                                                            </div>
                                                            <div className="form-group col-md-12 col-sm-12 col-xs-12">
                                                            <div className="field-label">Address</div>
                                                            <input type="text" name="streetAddress1"
                                                            value={this.state.streetAddress1}
                                                            onChange={this.setStateFromInput}
                                                            placeholder="Street address"/>
                                                            {this.validator.message('streetAddress1', this.state.streetAddress1, 'required|min:10|max:120')}
                                                            </div>
                                                            <div className="form-group col-md-12 col-sm-12 col-xs-12">
                                                            <div className="field-label">Town/City</div>
                                                            <input type="text" name="city" value={this.state.city}
                                                            onChange={this.setStateFromInput}/>
                                                            {this.validator.message('city', this.state.city, 'required|alpha')}
                                                            </div>
                                                            <div className="form-group col-md-12 col-sm-12 col-xs-12">
                                                            <div className="field-label">State/Province</div>
                                                            <input type="text" name="state" value={this.state.state}
                                                               onChange={this.setStateFromInput}/>
                                                            {this.validator.message('state', this.state.state, 'required|alpha')}
                                                            </div>
                                                            <div className="form-group col-md-12 col-sm-6 col-xs-12">
                                                            <div className="field-label">Postal Code</div>
                                                            <input type="text" name="postalCode"
                                                            value={this.state.postalCode}
                                                            onChange={this.setStateFromInput}/>
                                                            {this.validator.message('postalCode', this.state.postalCode, 'required|max:20')}
                                                            </div>
                                                            </div>

                                                            </>
                                                        )}
                                                        <br/>
                                                        <Mutation mutation={CHECKOUT_CREATE}>
                                                            {(checkoutCreate, { data }) => (
                                                            <button
                                                                type="button"
                                                                className="btn-solid btn"
                                                                disabled={showContact}
                                                                onClick={() => {
                                                                    let firstName,
                                                                        lastName,
                                                                        email,
                                                                        countryCode,
                                                                        city,
                                                                        postalCode,
                                                                        countryArea,
                                                                        streetAddress1;


                                                                    if(user.authenticated){
                                                                        email = me.email;
                                                                        firstName = me.firstName;
                                                                        lastName = me.lastName;
                                                                        streetAddress1 = me.defaultShippingAddress.streetAddress1;
                                                                        city = me.defaultShippingAddress.city;
                                                                        postalCode = me.defaultShippingAddress.postalCode;
                                                                        countryCode = me.defaultShippingAddress.country.code;
                                                                        countryArea = me.defaultShippingAddress.countryArea
                                                                    }else{
                                                                        if (this.validator.allValid()) {
                                                                            email = this.state.email;
                                                                            firstName = this.state.firstName;
                                                                            lastName = this.state.lastName;
                                                                            streetAddress1 = this.state.streetAddress1;
                                                                            city = this.state.city;
                                                                            postalCode = this.state.postalCode;
                                                                            countryCode = this.state.countryCode;
                                                                            countryArea = this.state.state;
                                                                        }else{
                                                                                this.validator.showMessages();
                                                                                this.forceUpdate();
                                                                                return false
                                                                            }

                                                                    }

                                                                    const lines = cartItems.map((cart)=>{
                                                                        let temp = {};
                                                                        temp.quantity = cart.qty;
                                                                        temp.variantId = cart.variantId;
                                                                        return temp;

                                                                    })

                                                                    checkoutCreate({
                                                                        variables:{
                                                                            input:{
                                                                                email:email,
                                                                                lines: lines,
                                                                                shippingAddress: {
                                                                                    firstName: firstName,
                                                                                    lastName: lastName,
                                                                                    streetAddress1: streetAddress1,
                                                                                    city: city,
                                                                                    postalCode: postalCode,
                                                                                    country: countryCode,
                                                                                    countryArea: countryArea,
                                                                                }
                                                                            }
                                                                        }
                                                                    }).then((res)=>{
                                                                        const {checkoutCreate} = res.data;
                                                                        if(checkoutCreate.errors.length === 0)

                                                                            this.setState({
                                                                                showCheckout:true,
                                                                                showContact:true,
                                                                                checkoutId:checkoutCreate.checkout.id,
                                                                                checkoutToken:checkoutCreate.checkout.token,
                                                                                shippingOptions:checkoutCreate.checkout.availableShippingMethods,
                                                                                errors:''
                                                                            });
                                                                        else{
                                                                            let errors = `<ul>`;
                                                                             checkoutCreate.errors.forEach((error)=>{
                                                                                 errors += `<li>${error.field}: ${error.message}</li>`
                                                                            });
                                                                            errors += `</ul>`;
                                                                            this.setState({errors:errors})
                                                                        }
                                                                    })
                                                                }}>
                                                                Continue
                                                            </button>
                                                            )}
                                                        </Mutation>
                                                    </div>
                                                    <div className="col-lg-6 col-sm-12 col-xs-12">
                                                        <div className={`checkout-details ${!this.state.showCheckout ? 'blur':''}`}>
                                                            <div className="order-box">
                                                                <div className="title-box">
                                                                    <div>Product <span> Total</span></div>
                                                                </div>
                                                                <ul className="qty">
                                                                    {cartItems.map((item, index) => {
                                                                        return <li key={index}>{item.name} × {item.qty}
                                                                            <span>{symbol} {item.sum}</span></li>
                                                                    })
                                                                    }
                                                                </ul>
                                                                <ul className="sub-total">
                                                                    <li>Subtotal <span
                                                                        className="count">{symbol}{total}</span></li>
                                                                    <li>Shipping <div className="shipping">
                                                                        <ShippingOptions data={shippingOptions} callback={(e)=>{
                                                                            console.log(e);
                                                                            this.setState({showCheckoutButton:false,shippingOptionId:e})}} />
                                                                    </div>
                                                                    </li>
                                                                </ul>

                                                                <ul className="total">
                                                                    <li>Total <span
                                                                        className="count">{symbol}{total}</span></li>
                                                                </ul>
                                                            </div>

                                                            <div className="payment-box">

                                                                {(total !== 0) ?
                                                                    <>
                                                                    <Mutation mutation={GET_STRIPE_SESSION_ID}>
                                                                        {(createPaymentSession, { data }) => (
                                                                        <Mutation mutation={SHIPPING_UPDATE}>
                                                                            {(checkoutShippingMethodUpdate, { data }) => (
                                                                                <div className="text-right">
                                                                                        <button type="button"
                                                                                                disabled={showCheckoutButton}
                                                                                                className="btn-solid btn"
                                                                                                onClick={() => {
                                                                                                   checkoutShippingMethodUpdate({
                                                                                                            variables: {
                                                                                                                checkoutId: checkoutId,
                                                                                                                shippingMethodId: shippingOptionId
                                                                                                            }
                                                                                                        }
                                                                                                    ).then((res)=>{

                                                                                                        let names = cartItems.map((cart)=>{
                                                                                                            return cart.name
                                                                                                        });
                                                                                                        console.log(names.join(', '));
                                                                                                        createPaymentSession({
                                                                                                            variables: {
                                                                                                                environment: process.env.REACT_APP_ENV,
                                                                                                                input: {
                                                                                                                    amount:res.data.checkoutShippingMethodUpdate.checkout.totalPrice.gross.amount,
                                                                                                                    quantity:1,
                                                                                                                    name:names.join(', '),
                                                                                                                    description:"Sneakers from Revendeur",
                                                                                                                    currency:"USD",
                                                                                                                    cancelUrl:"http://localhost:3000/checkout?failed=1",
                                                                                                                    successUrl:"http://localhost:3000/order-success",
                                                                                                                }

                                                                                                            }}).then(async (sessionData)=>{
                                                                                                            const stripe = await loadStripe(process.env.REACT_APP_STRIPE_API_KEY);
                                                                                                            localStorage.setItem('checkoutToken',checkoutToken);
                                                                                                            localStorage.setItem('strip_sessionId',sessionData.data.createPaymentSession.payment.token);
                                                                                                            stripe.redirectToCheckout({
                                                                                                                sessionId: sessionData.data.createPaymentSession.payment.token
                                                                                                                // sessionId: "cs_test_LTzjYO4f1Fqep48KjXnu1gMSDTuWx8o9icNQdveOGyVYDng6Mso9DWWC"
                                                                                                            }).then(function (result) {
                                                                                                                // If `redirectToCheckout` fails due to a browser or network
                                                                                                                // error, display the localized error message to your customer
                                                                                                                // using `result.error.message`.
                                                                                                            });
                                                                                                        })
                                                                                                    })

                                                                                                }}>Place
                                                                                            Order</button>
                                                                                </div>
                                                                            )}
                                                                            </Mutation>
                                                                        )}
                                                                    </Mutation>
                                                                        </>
                                                                    : ''}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        )
                    }}
                </Query>
            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    cartItems: state.cartList.cart,
    symbol: state.data.symbol,
    user: state.user,
    total: getCartTotal(state.cartList.cart)
})

export default connect(
    mapStateToProps,
    {removeFromWishlist}
)(checkOut)
