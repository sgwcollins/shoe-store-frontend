import React, {Component} from 'react';
import gql from 'graphql-tag';
import {Mutation, Query} from 'react-apollo';
import {SESSION_CHECK} from '../../constants/queries';
import store from '../../store';
import { clearCart } from '../../actions'

class orderSuccess extends Component {

    constructor (props) {
        super (props)

    }

    render (){

        console.log(localStorage.getItem('checkoutToken'));
        console.log(localStorage.getItem('strip_sessionId'));
        const strip_sessionId = localStorage.getItem('strip_sessionId');
        const checkoutToken = localStorage.getItem('checkoutToken');


        return (
            <Query
                query={SESSION_CHECK}
                variables={{
                    sessionId:strip_sessionId,
                    checkoutId:checkoutToken
                }}
            >
                {({ loading, error, data }) => {
                    if(!loading && !error) {
                        localStorage.removeItem('checkoutToken');
                        localStorage.removeItem('strip_sessionId');
                        store.dispatch(clearCart())
                    }
                    return (
                        <>
                            {loading &&(
                                <>
                                    <img style={{width:"150px",height:"150px",borderRadius:"200px",display:"block",margin:"auto"}} src='/assets/images/loading-sneaker.gif' />
                                </>
                            )}
                            {!loading &&(
                                <div>
                                    <section className="section-b-space light-layout">
                                        <div className="container">
                                            <div className="row">
                                                {!error && (
                                                    <div className="col-md-12">
                                                        <div className="success-text">
                                                            <i className="fa fa-check-circle" aria-hidden="true"></i>
                                                            <h2>thank you</h2>
                                                            <p>Payment Is Has Been Received Order Placed Successfully</p>
                                                            <p>Check the orders page to see your Order</p>
                                                        </div>
                                                    </div>
                                                )}
                                                {error && (
                                                    <div className="col-md-12">
                                                        <div className="error-section">
                                                            <h2>Something has gone wrong</h2>
                                                            <p>Please try to check again or contact us</p>
                                                        </div>
                                                    </div>
                                                )}

                                            </div>
                                        </div>
                                    </section>

                                </div>
                            )}
                        </>
                    );
                }}
            </Query>
        )
    }
}

export default orderSuccess
