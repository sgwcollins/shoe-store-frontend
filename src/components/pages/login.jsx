import React, {Component} from 'react';

import store  from '../../store/index';
import {setUserData} from '../../actions/user'
import Breadcrumb from "../common/breadcrumb";

import {LOGIN_MUTATION} from  '../../constants/queries'
import {Link} from 'react-router-dom';
import {List} from  '../../constants/util'
import { Mutation } from '@apollo/react-components';
import { get } from 'lodash';


class Login extends Component {

    constructor (props) {
        super (props)
        this.state = {
            email: '',
            password: '',
        }

    }



    render (){

        const {  email, password } = this.state

        return (
            <div>
                <Breadcrumb title={'Login'}/>


                {/*Login section*/}
                <section className="login-page section-b-space">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6">
                                <h3>Login</h3>
                                <div className="theme-card">
                                    <form className="theme-form">
                                        <div className="form-group">
                                            <label htmlFor="email">Email</label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="email"
                                                placeholder="Email"
                                                value={email}
                                                onChange={e => this.setState({ email: e.target.value })}
                                                required="" />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="review">Password</label>
                                            <input
                                                type="password"
                                                className="form-control"
                                                id="review"
                                                value={password}
                                                onChange={e => this.setState({ password: e.target.value })}
                                                placeholder="Enter your password"
                                                required="" />
                                        </div>
                                        <Mutation
                                            mutation={LOGIN_MUTATION}
                                            variables={{ email, password }}
                                            onCompleted={data => {

                                                if(data){
                                                    let token = get(data,'tokenCreate');
                                                    if(token['errors'].length > 0){
                                                        throw token['errors'];
                                                    }else{
                                                        this.confirm(token)
                                                    }
                                                }
                                            }}
                                            onError={
                                                data =>{
                                                    console.log(data)
                                                    return ('error has occurred');
                                                }
                                            }
                                        >
                                            {(mutation, { loading, error }) => (
                                                <div>
                                                <a className="btn btn-solid" onClick={mutation}>Logins</a>
                                                {loading && <p>Loading...</p>}
                                                    {error && <div className={'error-banner'} ><List list={error} key={'message'} /></div>}
                                                </div>

                                            )}
                                        </Mutation>
                                    </form>
                                </div>
                            </div>
                            <div className="col-lg-6 right-login">
                                <h3>New Customer</h3>
                                <div className="theme-card authentication-right">
                                    <h6 className="title-font">Create A Account</h6>
                                    <p>Sign up for a free account at our store. Registration is quick and easy. It
                                        allows you to be able to order from our shop. To start shopping click
                                        register.</p>
                                    <Link to={`${process.env.PUBLIC_URL}/pages/register`} className="btn btn-solid">Create an Account</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        )
    }

    confirm = async data => {

        store.dispatch(setUserData(data));
        this.props.history.push(`/`)
    }


}

export default Login
