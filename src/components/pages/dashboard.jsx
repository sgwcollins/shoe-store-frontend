import React, { useState, useEffect,useReducer } from 'react';
import { useSelector,useDispatch } from 'react-redux'
import Breadcrumb from "../common/breadcrumb";
import { useQuery } from '@apollo/react-hooks';
import Address from '../user/address';
import Tickets from '../user/tickets';
import ReloadTickets from '../user/reloadTickets';
import {GET_CUSTOMER_DETAILS} from '../../constants/queries';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import queryString from 'query-string'
import Modal from 'react-responsive-modal';
import {Personal} from './personal'
import {AddressForm} from './address'
import {Order} from './order'

function Dashboard(props) {

    const id  = useSelector(state => state.user.userData.id);
    const [tabIndex,setTabIndex ] = useState(0);
    const [openModal,setOpenModal ] = useState(false);
    const [activeModal,setActiveModal ] = useState('');


    const { loading, error, data, refetch} = useQuery(GET_CUSTOMER_DETAILS, {
        variables: { id },
    });
    if (loading) return 'Loading...';
    const values = queryString.parse(props.location.search);

    console.log('rerender');
    if('payment' in values && tabIndex !== 3) {
        console.log('---');
        setTabIndex(3);
    }

    const editContactModal = (name) =>{
        setOpenModal(true);
        setActiveModal(name)
    };

    const closeContactModal = () =>{
        setOpenModal(false);
        setActiveModal('');
        refetch()
    };

    if (loading) return 'Loading...';
    if (error) return `Error! ${error.message}`;

    const {
        firstName,
        lastName,
        email,
        ticketAmount,
        defaultShippingAddress = {
            streetAddress1 : ''
        },
        orders,
        usersRaffles
    } = data.me;

    // dispatch({type:type.SET_USER_DETAILS,data});

    return (
            <div>
                <Breadcrumb title={'Dashboard'}/>
                {/*Dashboard section*/}
                <Tabs selectedIndex={tabIndex} onSelect={tabIndex => setTabIndex(tabIndex)}>
                    <section className="section-b-space">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-3">
                                <div className="account-sidebar">
                                    <a className="popup-btn">
                                        my account
                                    </a>
                                </div>
                                <div className="dashboard-left">
                                    <div className="collection-mobile-back">
                                    <span className="filter-back">
                                        <i className="fa fa-angle-left" aria-hidden="true"></i> back
                                    </span>
                                    </div>
                                    <div className="block-content">
                                        <TabList>
                                            <Tab><a >Account Info</a></Tab>
                                            <Tab><a >My Orders</a></Tab>
                                            <Tab><a >My Tickets</a></Tab>
                                        </TabList>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-9">

                                <TabPanel>
                                    <div className="dashboard-right">
                                        <div className="dashboard">
                                            <div className="page-title">
                                                <h2>My Dashboard</h2>
                                            </div>
                                            <div className="box-account box-info">
                                                <div className="box-head">
                                                    <h2>Account Information</h2>
                                                </div>
                                                <div className="row">
                                                    <div className="col-sm-6">
                                                        <div className="box">
                                                            <div className="box-title">
                                                                <h3>Contact Information</h3>
                                                                <a onClick={()=>editContactModal('personal')}>Edit</a>
                                                            </div>
                                                            <div className="box-content">
                                                                {(firstName !== '' && lastName !=='') && <h6>{firstName} {lastName}</h6>}
                                                                <h6>{email}</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="box">
                                                        <div className="box-title">
                                                            <h3>Address Book</h3>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-sm-6">
                                                                <h6>Default Shipping Address</h6>
                                                                <Address
                                                                    address={defaultShippingAddress}
                                                                    openModal={(name)=>{editContactModal(name)}}
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </TabPanel>
                                <TabPanel>
                                    <Order orders={orders} />
                                </TabPanel>
                                <TabPanel>
                                    <Tickets
                                        ticketAmount={ticketAmount}
                                        tickets={usersRaffles}
                                        reloadTickets={()=>{setTabIndex(3)}}
                                    />
                                </TabPanel>
                                <TabPanel>
                                    <ReloadTickets {...props}/>
                                </TabPanel>

                            </div>
                        </div>
                    </div>
                </section>
                </Tabs>
                <Modal open={openModal} onClose={()=>setOpenModal(false)} center>
                    <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
                        <div className="modal-content quick-view-modal">
                            <div className="modal-body">

                                {activeModal === 'personal' && (
                                    <Personal
                                        data = {data.me}
                                        closeModal = {closeContactModal}
                                    />
                                )}

                                {activeModal === 'address' && (
                                    <AddressForm
                                        data = {data}
                                        closeModal = {closeContactModal}
                                    />
                                )}

                            </div>
                        </div>
                    </div>
                </Modal>

            </div>
        )
    }

export default Dashboard
