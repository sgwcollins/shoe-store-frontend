import React, {Component} from 'react';

import Breadcrumb from "../common/breadcrumb";
import {Mutation} from "@apollo/react-components";
import {SIGNUP_MUTATION} from "../../constants/queries";
import {get} from "lodash";
import {List} from "../../constants/util";

class Register extends Component {

    constructor (props) {
        super (props)
        this.state = {
            email: '',
            password: '',
        }

    }

    render (){

        const {  email, password } = this.state

        return (
            <div>
                <Breadcrumb title={'create account'}/>


                {/*Regsiter section*/}
                <section className="register-page section-b-space">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <h3>create account</h3>
                                <div className="theme-card">
                                    <form className="theme-form">
                                        <div className="form-row">
                                            <div className="col-md-4 offset-4  ">
                                                <label htmlFor="email">email</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    id="email"
                                                    value={email}
                                                    placeholder="Email"
                                                    required=""
                                                    onChange={e => this.setState({ email: e.target.value })}
                                                />

                                                <label htmlFor="review">Password</label>
                                                <input
                                                    type="password"
                                                    className="form-control"
                                                    id="review"
                                                    value={password}
                                                    placeholder="Enter your password"
                                                    required=""
                                                    onChange={e => this.setState({ password: e.target.value })}
                                                />
                                            </div>

                                        </div>
                                        <Mutation
                                            mutation={SIGNUP_MUTATION}
                                            variables={{ email, password }}
                                            onCompleted={data => {
                                                if(data){
                                                    let token = get(data,'accountRegister');
                                                    if(token['errors'].length > 0){
                                                        throw token['errors'];
                                                    }else{
                                                        this.confirm(token)
                                                    }
                                                }
                                            }}
                                        >
                                            {(mutation, { loading, error }) => (
                                                <div>
                                                    <a className="btn btn-solid offset-4" onClick={mutation}>create Account</a>
                                                    {loading && <p>Loading...</p>}
                                                    {error && <div className={'error-banner'} ><List list={error} key={'message'} /></div>}
                                                </div>

                                            )}
                                        </Mutation>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        )
    }

    confirm = () => {

        this.props.history.push(`/pages/login`)
    }

}

export default Register
